package main

import (
	"encoding/csv"
	"encoding/xml"
	"flag"
	"fmt"
	"image"
	_ "image/png"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var controlP = flag.String("control", "", "path to control file")
var pixelP = flag.String("pixel", "", "path to pixel .glif file")
var pixelHeightP = flag.Float64("height", 10, "nominal height of pixel")
var pixelWidthP = flag.Float64("width", 10, "nominal width of pixel")

func main() {
	flag.Parse()

	ps, _ := filepath.Glob("./*.ufo")
	if len(ps) != 1 {
		ps, _ = filepath.Glob("../*.ufo")
		if len(ps) != 1 {
			log.Fatal("Can't find *.ufo directory")
		}
	}
	root := ps[0]

	err := os.MkdirAll(filepath.Join(root, "glyphs"), 0755)
	if err != nil {
		log.Fatal(err)
	}

	glyphs := []*UFOGlyph{}

	// Collect glyphs
	for _, n := range flag.Args() {
		ug, err := ImageToCompositeGlyph(n,
			*pixelWidthP, *pixelHeightP)
		if err != nil {
			log.Print(err)
			continue
		}
		glyphs = append(glyphs, ug)
	}

	glyphControl, err := Control(*controlP)
	if err != nil {
		log.Fatal(err)
	}
	for _, glyph := range glyphs {
		name := glyph.Name
		m, ok := glyphControl[name]
		if !ok {
			continue
		}
		if m.Name != "" {
			glyph.Name = m.Name
		}
		if m.Unicode != "" {
			glyph.Unicode = append(glyph.Unicode, Unicode{Hex: m.Unicode[2:]})
		}
	}

	// Default to internal pixel...
	pixel := Pixel()
	// ... but use external one if given.
	if *pixelP != "" {
		externalPixel, err := PixelFromGlif(*pixelP)
		if err != nil {
			log.Print(err)
		} else {
			pixel = externalPixel
		}
	}

	glyphs = append(glyphs, pixel)

	WriteUFO(root, glyphs)
}

func WriteUFO(root string, glyphs []*UFOGlyph) {
	// Write to .glif files (and build map)
	// Ultimately used to make glyphs/contents.plist
	glyph_file_map := map[string]string{}
	for _, ug := range glyphs {
		w, err := os.OpenFile(filepath.Join(root, "glyphs", ug.Filepath()),
			os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			log.Print(err)
			continue
		}
		defer w.Close()
		bs, err := xml.MarshalIndent(ug, "", "  ")
		_, err = w.Write(bs)
		if err != nil {
			log.Print(err)
		}

		filename, occupied := glyph_file_map[ug.Name]
		if occupied {
			log.Printf("Glyph name [%s] has been duplicated in file %s and %s.\n",
				ug.Name, filename, ug.Filepath())
		} else {
			glyph_file_map[ug.Name] = ug.Filepath()
		}
	}

	w, err := os.OpenFile(filepath.Join(root, "glyphs", "contents.plist"),
		os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Print(err)
	} else {
		defer w.Close()
		MapToPlist(w, glyph_file_map)
	}
}

func ImageToCompositeGlyph(imagepath string, xscale, yscale float64) (*UFOGlyph, error) {
	Threshold := uint8(0)

	// Number of pixels from top of image to baseline.
	// Shared by "vec8,baseline" in CSV file
	ImageBaseline := 11.0

	r, err := os.Open(imagepath)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	img, _, err := image.Decode(r)

	gim := img.(*image.Gray)
	bounds := gim.Bounds()
	pixel_width := bounds.Max.X - bounds.Min.X
	advance_width := float64(pixel_width) * yscale

	glyph_name := filepath.Base(imagepath)
	glyph_name = strings.TrimSuffix(glyph_name, filepath.Ext(glyph_name))

	ug := NewUFOGlyph(glyph_name)
	ug.Advance.Width = &advance_width

	for y := bounds.Min.Y; y < bounds.Max.Y; y += 1 {
		for x := bounds.Min.X; x < bounds.Max.X; x += 1 {
			if gim.GrayAt(x, y).Y > Threshold {
				// x,y of bottom left
				blx := float64(x)
				bly := float64(y) + 1
				// Transform into FDU coordinates
				cx := blx * xscale
				cy := (ImageBaseline - bly) * yscale
				ug.Outline.Component =
					append(ug.Outline.Component, NewComponentAt("_pixel", cx, cy))
			}
		}
	}

	return &ug, nil
}

func Pixel() *UFOGlyph {
	ug := NewUFOGlyph("_pixel")
	ug.Outline.Contour = []Contour{
		Contour{
			Point: []Point{
				Point{X: 10, Y: 10, Type: "line"},
				Point{X: 0, Y: 10, Type: "line"},
				Point{X: 0, Y: 0, Type: "line"},
				Point{X: 10, Y: 0, Type: "line"},
			},
		},
	}
	width := 0.0
	ug.Advance.Width = &width

	return &ug
}

func PixelFromGlif(filepath string) (*UFOGlyph, error) {
	r, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	ug := UFOGlyph{}
	bs, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	err = xml.Unmarshal(bs, &ug)
	if err != nil {
		return nil, err
	}

	// By convention, the pixel is named _pixel
	ug.Name = "_pixel"
	// ... and has no unicode
	ug.Unicode = nil

	return &ug, nil
}

func MapToPlist(w io.Writer, cs map[string]string) {
	fmt.Fprintln(w, "<plist version=\"1.0\">")
	fmt.Fprintln(w, "<dict>")
	for k, v := range cs {
		fmt.Fprintf(w, "<key>%s</key>\n", k)
		fmt.Fprintf(w, "<string>%s</string>\n", v)
	}
	fmt.Fprintln(w, "</dict>")
	fmt.Fprintln(w, "</plist>")
}

type GlyphEntry struct {
	Name    string
	Unicode string
}

// from the control file at path, extra all the glyph control
// entries; which are of the form
// bufo,glyph,NAME,NEWNAME,UNICODE
// where:
//   - NAME is the glyph's current name (initially derived
//     from the PNG filename in this implementation);
//   - NEWNAME is the new name for the glyph (empty to not change)
//   - UNICODE is its Unicode value (empty to not assign)
func Control(path string) (map[string]GlyphEntry, error) {
	es := map[string]GlyphEntry{}
	if path == "" {
		return es, nil
	}
	r, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	csvr := csv.NewReader(r)
	csvr.FieldsPerRecord = -1

	rows, err := csvr.ReadAll()
	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		if row[0] == "bufo" && row[1] == "glyph" {
			name := row[2]
			entry := GlyphEntry{Name: row[3], Unicode: row[4]}
			es[name] = entry
		}
	}
	return es, nil
}

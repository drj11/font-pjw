# Font PJW

Font PJW is the font from Plan 9 (Pellucida) using
[PJW’s face](https://en.wikipedia.org/wiki/Peter_J._Weinberger) as
the "pixel".

The latest release is available on the [version/0.20240612 branch](https://gitlab.com/drj11/font-pjw/-/tree/version/0.20240612/fonts/ttf).
Versions that you may find on the `main` branch are in-progress
LAB specimens and should not be released.


## Building

Try

    make

`bufo` will convert PNG files to UFO.
Try something like this:

    bufo/bufo -control control.f8v $(find v -name \*.png)


UFO can be converted to TTF with:

    fontmake -o ttf -- pjw.ufo


## Status

Build via UFO.

- tx largely understands. output to PDF is ok
- makeotf makes a valid OTF but it has no cmap (think i may have
  broken this by removing various PostScript things from
  fontinfo.plist
- fontmake makes a file that seems to work, passes most
  fontbakery checks, can be installed, and so on.

# END
